# Introduzione
Realizzare un'applicazione per musei che permetta, tramite tecnologia beacons (estimote), la descrizione audio delle opere attraverso il proprio cellurare semplicemente avvicinandovisi. L'applicazione dovrà scaricare informazioni a partire da un database, queste informazioni dovranno essere gestite in maniera dinamica tramite un Backend.

# Stack
	- Swift (iOS App)
	- Vue.js & Babel (SPA)
	- Restify (Server)
