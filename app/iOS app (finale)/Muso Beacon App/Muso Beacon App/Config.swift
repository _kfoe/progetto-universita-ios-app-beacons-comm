//
//  Config.swift
//  BeaconAudio
//
//  Created by M on 23/07/17.
//  Copyright © 2017 M. All rights reserved.
//

import Foundation

class Config{
    struct SERVER {
        static let BASE = "http://665a9263.ngrok.io"
        static let LOGIN = SERVER.BASE + "/app/login"
        static let LISTA = SERVER.BASE + "/app/opere"
        static let SCARICATO = SERVER.BASE + "/app/opera/scaricata"
        static let VERIFY_UUID = SERVER.BASE + "/app/verifica/uuid"
        static let DOWNLOAD = SERVER.BASE + "/resource"
    }
    
    struct BEACON {
        static let UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"
        static let IDENTIFIER = "Estimotes"
    }
    
    struct BRACCIALE {
        static let UUID = ""
    }
    
    struct SEGUE {
        static let verso_info = ["loginviewcontroller": "login_to_info"]
        static let verso_login = ["infoviewcontroller": "info_to_login"]
        static let verso_beacon = ["infoviewcontroller": "info_to_beaconmap"]
    }
}
