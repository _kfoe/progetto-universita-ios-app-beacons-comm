//
//  Associazioni+CoreDataProperties.swift
//  Muso Beacon App
//
//  Created by M on 25/08/17.
//  Copyright © 2017 M. All rights reserved.
//

import Foundation
import CoreData


extension Associazioni {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Associazioni> {
        return NSFetchRequest<Associazioni>(entityName: "Associazioni")
    }

    @NSManaged public var identifier: String?
    @NSManaged public var minor: Int64
    @NSManaged public var thumb: String?
    @NSManaged public var audio: String?
    @NSManaged public var quadro: String?

}
