//
//  LoginViewController.swift
//  Muso Beacon App
//
//  Created by M on 30/07/17.
//  Copyright © 2017 M. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let current_uuid = UIDevice.current.identifierForVendor!.uuidString
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBAction func logga(_ sender: Any) {
        
        if (username.text != "" && password.text != ""){
        
        var d:[String:String] = [:]
        
        d["codice"] = username.text
        d["password"] = password.text
        d["uuid"] = current_uuid
        
        let request = Helpers().rq(method: "POST", uri: Config.SERVER.LOGIN, payload: d)
        
        Helpers().parse_json(completion: {
            
            jsonString in

            let errore = jsonString["errore"] as! Bool
            let info = jsonString["r"] as! String
            
            if (errore == true){
                
                let uiAlertController = UIAlertController(
                    title: "Errore",
                    message: info,
                    preferredStyle:.alert)
                
                uiAlertController.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (UIAlertAction) in uiAlertController.dismiss(animated: true, completion: nil)}))
                self.present(uiAlertController, animated: true, completion: nil)
                
                return
            }
        
            if (Helpers().primo_avvio()){
                let defaults = UserDefaults.standard
                defaults.set(true, forKey: "primo_avvio")
            }
            
            let defaults = UserDefaults.standard
            defaults.set(info, forKey: "token_jwt")
            
            let uiAlertController = UIAlertController(
                title: "Successo",
                message: info,
                preferredStyle:.alert)
            
            uiAlertController.addAction(UIAlertAction.init(title: "Prosegui", style: .default, handler: {
                
                (UIAlertAction) in self.performSegue(withIdentifier:
                    
                    Helpers().get_segue(controller: self.className, verso: "INFO")
                    
                    , sender: self)
            
            } ))
            
            self.present(uiAlertController, animated: true, completion: nil)
            
            
            
            
        }, request: request)
            
        }
        
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if(!Helpers().primo_avvio()){
            Helpers().need_refresh_jwt(controller: self)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //nascondo tastiera
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}
