//
//  Helpers.swift
//  Muso Beacon App
//
//  Created by M on 24/08/17.
//  Copyright © 2017 M. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Helpers: NSObject {
    
    var a = 0
    var z = 0

    internal override init() { }

    func setInterval(secondi: Int, completion: @escaping (Timer) -> ()) -> Void {
        _ = Timer.scheduledTimer(withTimeInterval: TimeInterval(secondi), repeats: true) { timer in completion(timer) }
    }

    func setTimeout(secondi: Int, completion: @escaping (Timer) -> ()) -> Void {
        _ = Timer.scheduledTimer(withTimeInterval: TimeInterval(secondi), repeats: false) { timer in completion(timer) }
    }

    func primo_avvio() -> Bool {
        if (UserDefaults.standard.object(forKey: "primo_avvio") == nil) {
            return true
        }
        return false
    }

    func rq(method: String, uri: String, payload: Dictionary<String, String>) -> URLRequest {

        var request = URLRequest(url: URL(string: uri)!)
        request.httpMethod = method
        if payload != [:] {

            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
            } catch let error {
                print("errore ", error.localizedDescription)
            }

            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")

        }

        if (UserDefaults.standard.object(forKey: "token_jwt") != nil) {

            let jwt = UserDefaults.standard.object(forKey: "token_jwt") as! String

            request.setValue("Bearer \(jwt)", forHTTPHeaderField: "Authorization")
        }

        return request
    }

    func parse_json(completion: @escaping (NSDictionary) -> (), request: URLRequest) {
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in

            if error != nil {
                print("Impossibile eseguire la richiesta HTTP!")
            }

            do {
                let r = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                completion(r)
            } catch let error as NSError {
                print("Impossibile eseguire il parse JSON!", error)
            }

        }
        task.resume()
    }


    func get_segue(controller: String, verso: String) -> String {

        if verso == "INFO" {
            return (Config.SEGUE.verso_info[controller])!
        } else if(verso == "LOGIN") {
            return (Config.SEGUE.verso_login[controller])!
        } else if(verso == "BEACON_MAP") {
            return (Config.SEGUE.verso_beacon[controller])!
        } else {
            print("segue sconosciuto")
            return ""
        }
    }

    func need_refresh_jwt(controller: UIViewController) -> Void {

        if (UserDefaults.standard.object(forKey: "token_jwt") != nil) {

            var d: [String: String] = [:]

            d["uuid"] = UIDevice.current.identifierForVendor!.uuidString
            d["jwt"] = UserDefaults.standard.object(forKey: "token_jwt") as! String

            let request = self.rq(method: "POST", uri: Config.SERVER.VERIFY_UUID, payload: d)

            self.parse_json(completion: {
                jsonString in

                print(jsonString)

                let errore = jsonString["errore"] as! Bool
                let info = jsonString["r"] as? String

                if (errore == true) {

                    let uiAlertController = UIAlertController(
                        title: "Errore",
                        message: info,
                        preferredStyle: .alert)

                    uiAlertController.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (UIAlertAction) in print(true) }))
                    controller.present(uiAlertController, animated: true, completion: nil)

                } else {

                    let uiAlertController = UIAlertController(
                        title: "Successo",
                        message: "Ho recuperato i dati di accesso",
                        preferredStyle: .alert)

                    uiAlertController.addAction(UIAlertAction.init(title: "Prosegui", style: .default, handler: { (UIAlertAction) in controller.performSegue(withIdentifier: self.get_segue(controller: controller.className, verso: "INFO"), sender: self) }))
                    controller.present(uiAlertController, animated: true, completion: nil)

                }

            }, request: request)

        }
    }

    func verifica_dump(controller: UIViewController, stato: UILabel, barra: UIProgressView) {

        var d: [String: String] = [:]

        d["uuid"] = UIDevice.current.identifierForVendor!.uuidString

        let request = self.rq(method: "POST", uri: Config.SERVER.LISTA, payload: d)

        stato.text = "Verifico esistenza dump"

        self.parse_json(completion: {

            jsonString in

            print(jsonString)

            let errore = jsonString["errore"] as! Bool

            if (errore == true) {

                let info = jsonString["r"] as! String

                if (controller.className != "loginviewcontroller") {

                    let uiAlertController = UIAlertController(
                        title: "Attenzione",
                        message: info,
                        preferredStyle: .alert)

                    uiAlertController.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: {

                        (UIAlertAction) in

                        controller.performSegue(withIdentifier:

                            self.get_segue(controller: controller.className, verso: "LOGIN")

                            , sender: self)

                    }))

                    controller.present(uiAlertController, animated: true, completion: nil)

                }

            } else {

                let data = jsonString["r"] as! [[String: AnyObject]]

                if (data.count <= 0) {
                    let uiAlertController = UIAlertController(
                        title: "Attenzione",
                        message: "Non esiste un dump da scaricare",
                        preferredStyle: .alert)

                    uiAlertController.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: {

                        (UIAlertAction) in

                        controller.performSegue(withIdentifier:

                            self.get_segue(controller: controller.className, verso: "BEACON_MAP")

                            , sender: self)


                    }))
                    controller.present(uiAlertController, animated: true, completion: nil)

                } else {

                    let uiAlertController = UIAlertController(
                        title: "Attenzione",
                        message: "Dump trovato",
                        preferredStyle: .alert)

                    uiAlertController.addAction(UIAlertAction.init(title: "Scarica", style: .default, handler: {

                        (UIAlertAction) in

                        print("Avvio download dump")
                        self.scarica_dump(controller: controller, dump: data, stato: stato, barra: barra)

                    }))
                    controller.present(uiAlertController, animated: true, completion: nil)
                }
            }

        }, request: request)

    }


    func scarica_dump(controller: UIViewController, dump: [[String: AnyObject]], stato: UILabel, barra: UIProgressView) {

        
        print(dump)
        
        for x in dump {

            var minor = 0
            var audio = ""
            var quadro = ""
            var thumb = ""
            var identifier = ""
            var hash_opera = ""

            if (x["minor"] != nil) {
                minor = x["minor"] as! Int
            }

            if (x["audio"] != nil) {
                audio = x["audio"] as! String
            }

            if (x["titolo"] != nil) {
                quadro = x["titolo"] as! String
            }

            if (x["thumb"] != nil) {
                thumb = x["thumb"] as! String
            }

            if (x["id_beacon"] != nil) {
                identifier = x["id_beacon"] as! String
            }

            if (x["hash"] != nil) {
                hash_opera = x["hash"] as! String
            }

            if (minor != 0 && identifier != "" && thumb != "" && hash_opera != "") {

                stato.text = "Scarico dump"

                //salvo nel db
                if(CoreDataAssociazioni.shared.get_data(minor: minor).count == 0) {
                    CoreDataAssociazioni.shared.salva(audio: audio, identifier: identifier, quadro: quadro, thumb: thumb, minor: minor)
                } else {
                    CoreDataAssociazioni.shared.aggiorna(audio: audio, identifier: identifier, quadro: quadro, thumb: thumb, minor: minor)
                }

                //salvo nella storage (se necessario)
                
                self.salva_audio(audio: audio, identifier: identifier, hash_opera: hash_opera, barra: barra)

                self.salva_thumb(thumb: thumb, identifier: identifier, hash_opera: hash_opera, barra: barra)
                
            }

        }
        
        
        self.setInterval(secondi: 3, completion: {
            
            (timer) in
            
            if(self.a == self.z){
                
                timer.invalidate()
                
                self.a = 0
                self.z = 0
                
                let uiAlertController = UIAlertController(
                    title: "Attenzione",
                    message: "Il dump è stato scaricato correttamente!",
                    preferredStyle: .alert)
                
                uiAlertController.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: {
                    
                    
                    (UIAlertAction) in
                    
                    controller.performSegue(withIdentifier:
                        
                        self.get_segue(controller: controller.className, verso: "BEACON_MAP")
                        
                        , sender: self)
                    
                    
                }))
                controller.present(uiAlertController, animated: true, completion: nil)

            }
            
        })

    }

    func get_data_storage(minor: Int) -> [String: String] {

        var array: [String: String] = [:]

        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

        let base = CoreDataAssociazioni.shared.get_data(minor: minor)

        if (base.count <= 0) {
            return array
        }
    
        if(base[0].quadro != nil && base[0].quadro != "null" && base[0].quadro != ""){
            array["titolo"] = base[0].quadro
        }else{
            array["titolo"] = ""
        }

        if (base[0].audio != nil && base[0].audio != "null" && base[0].audio != "") {

            let id_beacon = base[0].identifier!

            let audioServer = "audio/" + base[0].audio!

            let audioURL = id_beacon + "/" + audioServer

            let audioURLCompleta = documentsURL.appendingPathComponent(audioURL).path

            if FileManager.default.fileExists(atPath: audioURLCompleta) {
                array["audio"] = audioURLCompleta
            } else {
                array["audio"] = ""
            }

        }

        if (base[0].thumb != nil && base[0].thumb != "null" && base[0].thumb != "") {

            let id_beacon = base[0].identifier!

            let thumbServer = "thumb/" + base[0].thumb!

            let thumbURL = id_beacon + "/" + thumbServer

            let thumbURLCompleta = documentsURL.appendingPathComponent(thumbURL).path

            if FileManager.default.fileExists(atPath: thumbURLCompleta) {
                array["thumb"] = thumbURLCompleta
            } else {
                array["thumb"] = ""
            }

        }

        return array

    }

    private func salva_audio(audio: String, identifier: String, hash_opera: String, barra: UIProgressView) {

        if (audio != "" && audio != "null") {
            
            self.a += 1
            
            let file_finale = identifier + "/audio/" + audio
            let path_download = Config.SERVER.DOWNLOAD + "/" + hash_opera + "/" + audio

            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

                let fileURL = documentsURL.appendingPathComponent(file_finale)

                return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
            }

            Alamofire.download(path_download, to: destination)
                .downloadProgress { P in

                    barra.progress = Float(P.fractionCompleted)
                }
                .response {
                    response in
                    
                    //set opera scaricata
                    
                    var d: [String: String] = [:]
                    
                    d["hash_opera"] = hash_opera
                    d["uuid"] = UIDevice.current.identifierForVendor!.uuidString
                    
                    let request = self.rq(method: "POST", uri: Config.SERVER.SCARICATO, payload: d)
                    
                    self.parse_json(completion: {
                        
                        jsonString in
                        
                        print(jsonString)
                        
                    }, request: request)

                    self.z += 1
            }
        }
    }

    private func salva_thumb(thumb: String, identifier: String, hash_opera: String, barra: UIProgressView) {

        if (thumb != "" && thumb != "null") {
            
            self.a += 1

            let file_finale = identifier + "/thumb/" + thumb
            let path_download = Config.SERVER.DOWNLOAD + "/" + hash_opera + "/" + thumb

            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

                let fileURL = documentsURL.appendingPathComponent(file_finale)

                return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
            }

            Alamofire.download(path_download, to: destination)
                .downloadProgress { P in

                    barra.progress = Float(P.fractionCompleted)
                }
                .response {
                    response in
                    
                    //set opera scaricata
                    
                    var d: [String: String] = [:]
                    
                    d["hash_opera"] = hash_opera
                    d["uuid"] = UIDevice.current.identifierForVendor!.uuidString
                    
                    let request = self.rq(method: "POST", uri: Config.SERVER.SCARICATO, payload: d)
                    
                    self.parse_json(completion: {
                        
                        jsonString in
                        
                        print(jsonString)
                        
                    }, request: request)

                    self.z += 1
            }
        }
    }

}

//assegna variabile a controller per ritornare il relativo nome (lowercase)
extension NSObject {
    var className: String {
        return String(describing: type(of: self)).lowercased()
    }

    class var className: String {
        return String(describing: self).lowercased()
    }
}

