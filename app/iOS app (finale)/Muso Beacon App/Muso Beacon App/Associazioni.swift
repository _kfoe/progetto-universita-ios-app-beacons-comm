//
//  Associazioni+CoreDataClass.swift
//  Muso Beacon App
//
//  Created by M on 25/08/17.
//  Copyright © 2017 M. All rights reserved.
//

import Foundation
import CoreData

@objc(Associazioni)
public class Associazioni: NSManagedObject {
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
}
