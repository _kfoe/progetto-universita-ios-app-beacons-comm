import UIKit
import CoreLocation
import CoreBluetooth
import AVFoundation
import Foundation

class BeaconMapViewController: UIViewController, CLLocationManagerDelegate, CBCentralManagerDelegate, CBPeripheralDelegate {

    var permetti_verifica = true
    var verifica_avviata = false
    var last_minor = 0
    var avviso_stampato = false


    @IBOutlet weak var centrale: UILabel!

    let locationManager = CLLocationManager()
    var manager: CBCentralManager!
    var peripheral: CBPeripheral!
    let region = CLBeaconRegion(proximityUUID: UUID(uuidString: Config.BEACON.UUID)!, identifier: Config.BEACON.IDENTIFIER)

    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        manager = CBCentralManager(delegate: self, queue: nil)
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {

            DispatchQueue.main.async(execute: {

                //self.performSegue(withIdentifier: "login_to_permessigps", sender: self)
                print("richiedi consenso uso gps")

            })


            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }


        locationManager.startRangingBeacons(in: region)

        centrale.text = "In attesa di beacon..."

    }

    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {

        let knownBeacons = beacons.filter { $0.proximity != CLProximity.unknown }


        if (knownBeacons.count == 1) {

            let b = knownBeacons[0] as CLBeacon

            let currentMinor = Int(b.minor)

            if (!verifica_avviata && permetti_verifica == true && last_minor != currentMinor && Helpers().get_data_storage(minor: currentMinor) != [:] && navigationController!.visibleViewController!.className != "beacondetailviewcontroller") {

                verifica_avviata = true
                last_minor = currentMinor

                let myVC = storyboard?.instantiateViewController(withIdentifier: "BeaconDetail") as! BeaconDetailViewController

                myVC.data_storage_ricevuto = Helpers().get_data_storage(minor: Int(b.minor))


                navigationController?.pushViewController(myVC, animated: true)

            }

        } else if(knownBeacons.count > 1) {

            if (!self.avviso_stampato) {

                self.avviso_stampato = true
                
                //forzo un non fetch!
                self.verifica_avviata = false
                self.permetti_verifica = false
                
                if (self.navigationController!.visibleViewController!.className == "beacondetailviewcontroller") {
                    self.navigationController!.visibleViewController!.performSegue(withIdentifier: "detail_to_beaconmap", sender: self)
                }

                let uiAlertController = UIAlertController(
                    title: "Attenzione",
                    message: "È stato rilevato più di un beacon. Allontarsi e riprovare!",
                    preferredStyle: .alert)

                uiAlertController.addAction(UIAlertAction.init(title: "Va bene", style: .default, handler: {

                    (UIAlertAction) in
                    
                    self.avviso_stampato = false
                    self.permetti_verifica = true
                    
                }))

                navigationController!.visibleViewController!.present(uiAlertController, animated: true, completion: nil)

            }

        } else {

            if (verifica_avviata == true && permetti_verifica == true) {

                verifica_avviata = false

                last_minor = 0

                if (navigationController!.visibleViewController!.className == "beacondetailviewcontroller") {
                    navigationController!.visibleViewController!.performSegue(withIdentifier: "detail_to_beaconmap", sender: self)
                }

            }

            print("Nessun beacons in range")

        }

    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {

        func centralManagerDidUpdateState(_ central: CBCentralManager) {
            if central.state == CBManagerState.poweredOn {
                central.scanForPeripherals(withServices: nil, options: nil)
            } else {
                print("Bluetooth non disponibile")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

}
