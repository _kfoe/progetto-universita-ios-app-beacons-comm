//
//  BeaconDetailViewController.swift
//  Muso Beacon App
//
//  Created by M on 26/08/17.
//  Copyright © 2017 M. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

class BeaconDetailViewController: UIViewController {

    var player_play = false
    var playerItem: AVPlayerItem?
    var current_player: AVPlayer?

    @IBOutlet weak var play_pause: UIImageView!
    @IBOutlet weak var titolo: UILabel!
    var data_storage_ricevuto: [String: String] = [:]

    @IBAction func pause_play_action(_ sender: Any) {

        if(player_play == false) {
            self.playerItem = AVPlayerItem(url: URL.init(fileURLWithPath: self.data_storage_ricevuto["audio"]!))
            self.current_player = AVPlayer(playerItem: self.playerItem!)
            self.current_player!.play()
            self.player_play = true
            play_pause.image = UIImage(named: "pause")

        } else {

            self.current_player!.pause()
            self.player_play = false
            play_pause.image = UIImage(named: "play")

        }

    }

    @IBOutlet weak var thumb: UIImageView!

    override func viewDidLoad() {

        thumb.addSubview(titolo)
        thumb.addSubview(play_pause)

        super.viewDidLoad()


        if(self.player_play == true) {
            self.current_player!.pause()
            self.player_play = false
        }

        titolo.text = self.data_storage_ricevuto["titolo"]
        thumb.image = UIImage(contentsOfFile: self.data_storage_ricevuto["thumb"]!)
        play_pause.image = UIImage(named: "play")

        Helpers().setInterval(secondi: 1, completion: {

            (timer) in

            if(self.current_player?.rate != 0 && self.current_player?.error == nil) {
                print("in riproduzione")
            } else {

                self.play_pause.image = UIImage(named: "play")
                self.player_play = false
                
            }

            if(self.navigationController!.visibleViewController!.className == "beaconmapviewcontroller") {
                
                timer.invalidate()

                if (self.player_play == true) {

                    self.current_player!.pause()
                    self.player_play = false

                }
            }

        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override var prefersStatusBarHidden: Bool {
        return true
    }
}
