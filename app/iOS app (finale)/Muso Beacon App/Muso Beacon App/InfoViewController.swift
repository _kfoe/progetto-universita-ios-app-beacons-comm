//
//  InfoViewController.swift
//  Muso Beacon App
//
//  Created by M on 25/08/17.
//  Copyright © 2017 M. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var centrale: UILabel!
    @IBOutlet weak var barra_progresso: UIProgressView!
    
    static let shared = InfoViewController()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        Helpers().verifica_dump(controller: self, stato: centrale, barra: barra_progresso)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
