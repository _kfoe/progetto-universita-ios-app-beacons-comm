//
//  CoreDataAssociazioni.swift
//  Muso Beacon App
//
//  Created by M on 25/08/17.
//  Copyright © 2017 M. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataAssociazioni {
    
    static let shared = CoreDataAssociazioni()
    
    private var context: NSManagedObjectContext
    
    private init() {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
    }

    func salva(audio: String, identifier: String, quadro: String, thumb: String, minor: Int){
        
        let entity = NSEntityDescription.entity(forEntityName: "Associazioni", in: self.context)
        
        let nuovo = Associazioni(entity: entity!, insertInto: self.context)
        
        nuovo.audio = audio
        nuovo.identifier = identifier
        nuovo.quadro = quadro
        nuovo.thumb = thumb
        nuovo.minor = Int64(minor)
        
        do {
            try self.context.save()
            
            print("Associazione salvata")
        } catch let errore {
            print("errore: \n \(errore) \n")
        }
    
        
    }
    
    func get_data(minor: Int) -> [Associazioni] {
        
        let request: NSFetchRequest<Associazioni> = NSFetchRequest(entityName: "Associazioni")
        request.returnsObjectsAsFaults = false
        
        let predicate = NSPredicate(format: "minor = %i", minor)
        request.predicate = predicate
        
        let books = self.fetch_request(request: request)
        
        guard books.count > 0 else {
        
            return []
        }
        
    
        return [books[0]]
        
    }
    
    func aggiorna(audio: String, identifier: String, quadro: String, thumb: String, minor: Int){
        
        if (self.get_data(minor: minor).count == 1){
            
            let d = self.get_data(minor: minor)[0]
            
            d.setValue(audio, forKey: "audio")
            d.setValue(identifier, forKey: "identifier")
            d.setValue(quadro, forKey: "quadro")
            d.setValue(thumb, forKey: "thumb")
            d.setValue(Int64(minor), forKey: "minor")
            
            do {
                try context.save()
                print("record aggiornato!")
            } catch let error as NSError  {
                print("impossibile salvare \(error), \(error.userInfo)")
            }
            
        }else{
            print("Impossibile aggiornare, non esiste il record")
        }
    
        
    }
    
    private func fetch_request(request: NSFetchRequest<Associazioni>) -> [Associazioni] {
        var array = [Associazioni]()
        do {
            array = try self.context.fetch(request)
            

            guard array.count > 0 else {
                return []
            }
            
        } catch let errore {
            print("errore: \n \(errore) \n")
        }
        return array
    }

}
