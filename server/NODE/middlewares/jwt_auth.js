const jwt = require('../helpers/jwt.js')


const jwt_middleware = (req, res, next) => {
  var v = ""
  if (req.getPath() == '/private/login' || /resource/i.test(req.getPath()) || req.getPath() == '/app/login') {
    next()
    return
  }

  if (/^\/private/.test(req.getPath())) {
    v = jwt.verifica(req, "PRIVATE")
  } else if (/^\/app/.test(req.getPath())) {
    v = jwt.verifica(req, "APP")
  }

  if (v != true) {
    res.send(v.http, v)
    return
  }

  next()
}

module.exports = jwt_middleware