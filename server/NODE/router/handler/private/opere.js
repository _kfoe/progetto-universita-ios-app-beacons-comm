const model = require('../../../models/private/opere.js')
const file = require('../../../helpers/file.js')
const response = require('../../../config/responses/error.js')
const crypto = require('crypto')
const q = require('q')

const get_opere = (req, res, next) => {
  model.get_opere()
    .then((response) => {
      res.send(response.http, response)
    })
}

const del_opera = (req, res, next) => {
  model.del_opera(req.params.hash)
    .then((response) => {
      res.send(response.http, response)
    })
}

const new_opera = (req, res, next) => {

  const hash_opera = crypto.randomBytes(20).toString('hex');

  req.params['hash'] = hash_opera

  model.exists_opera(hash_opera)
    .then((data) => {
      if (data == false) {
        return model.new_opera([req.files, req.params])
      } else {
        res.send(response.E_SERVER.http, response.E_SERVER)
        return $q.reject('Hash già esistente')
      }
    })
    .then((response) => {
      res.send(response.http, response)
    })
    .catch((e) => {
      console.log(e)
    })

}

const update_opera = (req, res, next) => {

  model.exists_opera(req.params.hash)
    .then((data) => {
      if (data == false) {
        res.send(response.ERR_OPERA.http, response.ERR_OPERA)
        return $q.reject('Impossibile aggiornare, l\'opera non esiste')
      } else {
        return model.update_opera([req.files, req.params])
      }
    })
    .then((response) => {
      res.send(response.http, response)
    })
    .catch((e) => {
      console.log(e)
    })

}

module.exports = {
  get_opere,
  del_opera,
  new_opera,
  update_opera
}