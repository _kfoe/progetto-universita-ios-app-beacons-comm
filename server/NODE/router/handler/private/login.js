const model = require('../../../models/private/login.js')

const login = (req, res, next) => {
  model.login(req.params.username, req.params.password)
    .then((response) => {
      res.send(response.http, response)
    })
}

module.exports = {
  login
}