const model = require('../../../models/private/beacons.js')
const _ = require('lodash')
const get_beacon = (req, res, next) => {
  model.get_beacon()
    .then((response) => {
      res.send(response.http, response)
    })
}

const aggiorna_data_db = (req, res, next) => {

  model.get_beacon()
    .then((current_db) => {
      if (current_db.r.length <= 0) {
        res.end()
        return $q.reject('Nessun beacon presente nel db. Quindi non aggiorno')
      } else {

        const current_data_db = _.map(current_db.r, function(o) {
          return _.pick(o, ['minor', 'major', 'identifier', 'hash_opera'])
        })

        return model.aggiorna_data_db(req.params, current_data_db)
      }
    })
    .then((response) => {
      res.send(response.http, response)
    })
    .catch((e) => {
      console.log(e)
    })
}


module.exports = {
  get_beacon,
  aggiorna_data_db
}