const model = require('../../../models/private/accessi_app.js')

const get_accessi = (req, res, next) => {
  model.get_accessi()
    .then((response) => {
      res.send(response.http, response)
    })
}

const aggiorna = (req, res, next) => {
  model.aggiorna(req.params.id, req.params.flag)
    .then((response) => {
      res.send(response.http, response)
    })
}

const cancella = (req, res, next) => {
  model.cancella(req.params.id)
    .then((response) => {
      res.send(response.http, response)
    })
}

module.exports = {
  get_accessi,
  aggiorna,
  cancella
}