const model = require('../../../models/app/generic.js')

const login = (req, res, next) => {
  model.login(req.params.codice, req.params.password, req.params.uuid)
    .then((response) => {
      res.send(response.http, response)
    })
}

const verifica_uuid_ban = (req, res, next) => {
  model.verifica_uuid_ban(req.params.uuid)
    .then((response) => {
      res.send(response.http, response)
    })
}

const get_opere = (req, res, next) => {
  model.get_opere(req.params.uuid)
    .then((response) => {
      res.send(response.http, response)
    })
}

const set_scaricato = (req, res, next) => {
  model.set_scaricato(req.params.hash_opera, req.params.uuid)
    .then((response) => {
      res.send(response.http, response)
    })
}

module.exports = {
  get_opere,
  set_scaricato,
  login,
  verifica_uuid_ban
}