const Router = require('restify-router').Router;
const routerInstance = new Router();
const routes = require('../config/route.js')

routes.forEach((x) => {
  routerInstance[x.method](x.path, x.rhandler)
})

module.exports = {
  routerInstance
}