const config = require('./../config/app.js')
const mysql = require('mysql')

const connetti = () => {

  const connection = mysql.createConnection({
    host: config.db_localhost,
    user: config.db_username,
    password: config.db_password,
    port: config.db_port,
    database: config.db_name
  });

  connection.connect(function(err) {
    if (err) {
      console.error('errore connessione db: ' + err.stack);
    }
    console.log('database connesso ' + connection.threadId);
  });

  return connection;
}

module.exports = connetti