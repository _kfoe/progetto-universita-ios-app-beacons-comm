const config = require('./../config/app.js')
const bcrypt = require('bcrypt')

const genera = (data) => {
  if (data) {
    return bcrypt.hashSync(data, bcrypt.genSaltSync(10));
  }
}

const verifica = (data, hash) => {
  if (data && hash) {
    return bcrypt.compareSync(data, hash);
  }
}

module.exports = {
  genera,
  verifica
}