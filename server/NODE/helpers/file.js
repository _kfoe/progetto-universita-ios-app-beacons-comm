const config = require('./../config/app.js')
const pathExists = require('path-exists');
const mime = require('mime-types');
const _ = require('lodash')
const fs = require('fs')
const crypto = require('crypto')
const rmdir = require('rmdir');

const allow_audio = ["mp3"]
const allow_image = ["png", "jpeg", "jpg"]

const add_file = (dir, file, nf) => {

  const ext = mime.extension(file.type)

  if (!file || !dir) {
    return
  }

  if (!_.includes(allow_audio, ext) && !_.includes(allow_image, ext)) {
    return
  }

  if (!pathExists.sync(dir)) {
    fs.mkdir(dir)
  }

  const path_avvio = file.path

  const path_finale = dir + '/' + nf

  const source = fs.createReadStream(path_avvio);
  const dest = fs.createWriteStream(path_finale);

  source.pipe(dest);
  source.on('end', function() {
    console.log('download ok')
  });
  source.on('error', function(err) {
    console.log(err)
  });

}

const del_dir = (dir) => {
  rmdir(dir);
}

module.exports = {
  add_file,
  del_dir
}