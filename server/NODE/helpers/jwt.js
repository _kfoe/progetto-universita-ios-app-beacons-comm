var jwt = require('jsonwebtoken')
const config = require('./../config/app.js')
const errori = require('../config/responses/error.js')



const genera = (type) => {
  var secret = ""

  if (type == "APP") {
    secret = config.secret_jwt_app
  } else if (type == "PRIVATE") {
    secret = config.secret_jwt_private
  } else {
    secret = "false"
  }

  token = jwt.sign({}, secret, {
    expiresIn: config.jwt_exp + 'h'
  });

  var b = new Buffer(token);
  return b.toString('base64');
}

const verifica = (req, type) => {

  var secret = ""

  if (!req.headers.authorization) {
    return errori.NO_BEARER
  }

  const s = req.headers.authorization.split(' ')

  if (!/^Bearer$/i.test(s[0])) {
    return errori.AUTH_FORMAT
  }

  try {

    var b = new Buffer(s[1], 'base64');
    var token_decodebase64 = b.toString();

    if (type == "APP") {
      secret = config.secret_jwt_app
    } else if (type == "PRIVATE") {
      secret = config.secret_jwt_private
    } else {
      secret = "false"
    }

    jwt.verify(token_decodebase64, secret);

    return true

  } catch (err) {
    if (err.name == "TokenExpiredError") {
      return errori.JWT_EXP
    } else {
      return errori.JWT_ERR
    }
  }
}

module.exports = {
  verifica,
  genera
}