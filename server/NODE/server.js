global.$db = require('./helpers/db.js')()
global.$q = require('q')

const rotte = require('./router/routes.js')
const restify = require('restify');
const jwtmiddle = require('./middlewares/jwt_auth.js')
const logger = require('morgan')

var server = restify.createServer({
  name: '[MUSO]'
});

//permetti options request (da migliorare, creare cors più adeguato)
server.opts(/.*/, function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8080");
  res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
  res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
  res.send(200);
  return next();
});

server.use((req, res, next) => jwtmiddle(req, res, next))
server.use(logger('dev'));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser({
  mapParams: true
}));
server.use(restify.plugins.bodyParser({
  mapParams: true
}));


server.get('/resource\/.*/', restify.plugins.serveStatic({
  'directory': './public',
  'default': 'index.html'
}));

rotte.routerInstance.applyRoutes(server);

server.listen(4113, function() {
  console.log('%s listening at %s', server.name, server.url);
});