const _ = require('lodash')
const rotte_app = require('./routes/app/routes.js')
const rotte_private = require('./routes/private/routes.js')

const routes = _.union(rotte_app, rotte_private)

module.exports = routes