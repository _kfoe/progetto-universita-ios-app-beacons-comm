const login_handler = require('../../../router/handler/private/login.js')
const beacon_handler = require('../../../router/handler/private/beacons.js')
const opere_handler = require('../../../router/handler/private/opere.js')
const accessi_private_handler = require('../../../router/handler/private/accessi_private.js')
const accessi_app_handler = require('../../../router/handler/private/accessi_app.js')
const routes = [{
  path: 'private/login',
  method: 'post',
  rhandler: login_handler.login
}, {
  path: 'private/beacons',
  method: 'get',
  rhandler: beacon_handler.get_beacon
}, {
  path: '/private/beacons/db',
  method: 'post',
  rhandler: beacon_handler.aggiorna_data_db
}, {
  path: 'private/opere',
  method: 'get',
  rhandler: opere_handler.get_opere
}, {
  path: 'private/opera\/([a-z-A-Z0-9]+)/',
  method: 'del',
  rhandler: opere_handler.del_opera
}, {
  path: 'private/opera/nuova',
  method: 'post',
  rhandler: opere_handler.new_opera
}, {
  path: 'private/opera/aggiorna',
  method: 'put',
  rhandler: opere_handler.update_opera
}, {
  path: 'private/accessi_sito',
  method: 'get',
  rhandler: accessi_private_handler.get_accessi
}, {
  path: 'private/accessi_sito/aggiorna',
  method: 'put',
  rhandler: accessi_private_handler.aggiorna
}, {
  path: 'private/accessi_sito/cancella',
  method: 'del',
  rhandler: accessi_private_handler.cancella
}, {
  path: 'private/accessi_app',
  method: 'get',
  rhandler: accessi_app_handler.get_accessi
}, {
  path: 'private/accessi_app/aggiorna',
  method: 'put',
  rhandler: accessi_app_handler.aggiorna
}, {
  path: 'private/accessi_app/cancella',
  method: 'del',
  rhandler: accessi_app_handler.cancella
}]

module.exports = routes