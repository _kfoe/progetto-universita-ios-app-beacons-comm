const app_handler = require('../../../router/handler/app/generic.js')

const routes = [{
  path: 'app/login',
  method: 'post',
  rhandler: app_handler.login
},{
  path: 'app/verifica/uuid',
  method: 'post',
  rhandler: app_handler.verifica_uuid_ban
}, {
  path: 'app/opere',
  method: 'post',
  rhandler: app_handler.get_opere
}, {
  path: 'app/opera/scaricata',
  method: 'post',
  rhandler: app_handler.set_scaricato
}]

module.exports = routes