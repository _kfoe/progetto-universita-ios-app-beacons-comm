const error = {
  E_SERVER: {
    http: 500,
    errore: true,
    r: 'Errore server.',
    code: 'ERRSRV'
  },
  NO_BEARER: {
    http: 401,
    errore: true,
    r: "Non è presente il Bearer token nell'header",
    code: "AUTH1"
  },
  AUTH_FORMAT: {
    http: 401,
    errore: true,
    r: "Formato authorization necessario è Bearer, altri tipi non consentiti!",
    code: "AUTH2"
  },
  JWT_EXP: {
    http: 401,
    errore: true,
    r: "Il token è scaduto",
    code: "JWTEXP"
  },
  JWT_ERR: {
    http: 401,
    errore: true,
    r: "Il token non è valido",
    code: "JWTERR"
  },
  ERR_LOGIN: {
    http: 200,
    errore: true,
    r: "Username o password non validi",
    code: "ERRLOGIN"
  },
  ERR_ALLOW_APP: {
    http: 200,
    errore: true,
    r: "La tua autorizzazione è stata revocata!",
    code: "ERALLOWAPP"
  },
  ERR_DEL_OPERA: {
    http: 200,
    errore: true,
    r: "Impossibile rimuovere l'opera",
    code: "ERRDEOPE"
  },
  ERR_OPERA: {
    http: 200,
    errore: true,
    r: "L'opera non esiste",
    code: "ERROPE"
  }
}

module.exports = error