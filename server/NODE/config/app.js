const config = {

  //per db
  db_username: 'root',
  db_password: 'root',
  db_localhost: 'localhost',
  db_name: 'projectunicam',
  db_port: 8889,
	
  //per jwt app
  secret_jwt_app: "",

  //per jwt sito
  secret_jwt_private: "",

  jwt_exp: 10 //in ore

};

module.exports = config