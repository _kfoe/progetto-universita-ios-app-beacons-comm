const response = require('../../config/responses/error.js')
const jwt = require('../../helpers/jwt.js')
var bcrypt = require('../../helpers/bcrypt.js');

const get_opere = (uuid) => {
  const deferred = $q.defer()

  const sql = "SELECT dump_scaricati.uuid_iphone, A.titolo, A.id_beacon, A.thumb, A.audio, A.hash, beacons.major, beacons.minor FROM dump_scaricati" +
    " RIGHT JOIN ( " +
    " SELECT opere.* FROM opere" +
    " ) AS A" +
    " ON dump_scaricati.hash_opera = A.hash AND dump_scaricati.uuid_iphone = ?" +
    " RIGHT JOIN beacons" +
    " ON beacons.identifier = A.id_beacon" +
    " WHERE dump_scaricati.uuid_iphone IS NULL AND (A.id_beacon != null || A.id_beacon != \"null\")"

  $db.query(sql, [uuid], (error, results, fields) => {
    if (error) {
      console.log('[errore] model/opere_app', error)
      deferred.resolve(response.E_SERVER)
    } else {
      deferred.resolve({
        http: 200,
        errore: false,
        r: results,
      })
    }
  })

  return deferred.promise
}



const set_scaricato = (hash_opera, uuid) => {
  const deferred = $q.defer()

  $db.query("SELECT COUNT(hash_opera) as totale FROM dump_scaricati WHERE hash_opera = ? AND uuid_iphone = ?", [hash_opera, uuid], (error, results, fields) => {

    results = Object.assign({}, results[0])
    console.log(results)
    if (results.totale == 0) {
      $db.query('INSERT INTO dump_scaricati(hash_opera, uuid_iphone) VALUES(?,?)', [hash_opera, uuid], (e, r, f) => {
        if (e) {
          console.log('[errore] model/opere_app', e)
          deferred.resolve(response.E_SERVER)
        } else {
          deferred.resolve({
            http: 200,
            errore: false,
            r: "Dump scaricato aggiunto correttamente!",
          })
        }
      })
    } else {
      deferred.resolve({
        http: 200,
        errore: false,
        r: "Dump scaricato già aggiunto!",
      })
    }
  })

  return deferred.promise
}

const login = (codice, password, uuid) => {
  const deferred = $q.defer();

  console.log(codice, password, uuid)

  if (uuid == "" || uuid == null || uuid == undefined) {
    console.log('[errore] model/login_app: Non esiste uuid')
    deferred.resolve({
      http: 200,
      errore: true,
      r: "Impossibile procedere, non è stato fornito id"
    })
  } else {

    $db.query('SELECT password, is_bannato FROM accessi_app WHERE codice = ?', [codice], (error, results, fields) => {
      if (error) {
        console.log('[errore] model/login_app', error)
        deferred.resolve(response.E_SERVER)
      } else {
        if (results.length <= 0) {
          deferred.resolve(response.ERR_LOGIN)
        }

        results = Object.assign({}, results[0])

        if (results.is_bannato == 1) {
          deferred.resolve(response.ERR_ALLOW_APP)
        } else if (!bcrypt.verifica(password, results.password)) {
          deferred.resolve(response.ERR_LOGIN)
        } else {

          $db.query("UPDATE accessi_app SET ultimo_accesso = NOW(), ?", {
            last_uuid: uuid
          })

          deferred.resolve({
            http: 200,
            errore: false,
            r: jwt.genera("APP")
          })

        }
      }
    })

  }
  return deferred.promise
}

const verifica_uuid_ban = (uuid) => {
  const deferred = $q.defer()

  if (uuid == "" || uuid == null || uuid == undefined) {
    console.log('[errore] model/verifica_uuid_login_app: Non esiste uuid')
    deferred.resolve({
      http: 200,
      errore: true,
      r: "Impossibile procedere, non è stato fornito id"
    })

    return deferred.promise
  }

  $db.query("SELECT is_bannato FROM accessi_app WHERE last_uuid = ?", [uuid], (error, results, fields) => {
    if (error) {
      console.log('[errore] model/login_app', error)
      deferred.resolve(response.E_SERVER)
    } else {
      if (results.length <= 0) {
        deferred.resolve(response.ERR_LOGIN)
      } else {
        results = Object.assign({}, results[0])
        if (results.is_bannato == 1) {
          deferred.resolve(response.ERR_ALLOW_APP)
        } else {
          deferred.resolve({
            http: 200,
            errore: false,
            r: "UUID non bannato"
          })
        }
      }
    }
  })

  return deferred.promise
}

module.exports = {
  get_opere,
  set_scaricato,
  login,
  verifica_uuid_ban
}