const response = require('../../config/responses/error.js')

const get_accessi = () => {
  const deferred = $q.defer()

  $db.query("SELECT ultimo_accesso, is_bannato, codice, last_uuid, id FROM accessi_app ORDER BY is_bannato DESC", (error, results, fields) => {
    if (error) {
      console.log('[errore] model/postazioni', error)
      deferred.resolve(response.E_SERVER)
    } else {
      deferred.resolve({
        http: 200,
        errore: false,
        r: results,
      })
    }
  })

  return deferred.promise
}

const aggiorna = (id, flag) => {
  const deferred = $q.defer()

  const ds = $db.query("UPDATE accessi_app SET is_bannato = ? WHERE id = ? ", [flag, id], (error, results, fields) => {
    if (error) {
      console.log('[errore] model/postazioni', error)
      deferred.resolve(response.E_SERVER)
    } else {
      deferred.resolve({
        http: 200,
        errore: false,
        r: "Postazione aggiornata con successo!",
      })
    }

  })

  return deferred.promise
}

const cancella = (id) => {
  const deferred = $q.defer()

  const ds = $db.query("DELETE FROM accessi_app WHERE id = ?", [id], (error, results, fields) => {
    if (error) {
      console.log('[errore] model/postazioni', error)
      deferred.resolve(response.E_SERVER)
    } else {
      deferred.resolve({
        http: 200,
        errore: false,
        r: "Postazione rimossa con successo!",
      })
    }
  })

  return deferred.promise
}

module.exports = {
  get_accessi,
  aggiorna,
  cancella
}