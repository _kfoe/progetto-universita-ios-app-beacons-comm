const response = require('../../config/responses/error.js')
const _ = require('lodash')

const get_beacon = () => {
  const deferred = $q.defer()

  $db.query('SELECT identifier, minor, major, hash_opera FROM beacons WHERE identifier != "null" AND identifier IS NOT NULL', (error, results, fields) => {
    if (error) {
      console.log('[errore] model/beacon', error)
      deferred.resolve(response.E_SERVER)
    } else {

      _.map(results, function(p) {
        p['disabled'] = true
      })

      deferred.resolve({
        http: 200,
        errore: false,
        r: results,
      })
    }
  })

  return deferred.promise
}

const aggiorna_data_db = (data_server, data_current_db) => {
  const deferred = $q.defer()

  var dove_cerco = false

  if (data_server.length <= 0) {
    deferred.resolve({
      http: 200,
      errore: false,
      r: "Nessun beacon da aggiornare nel db"
    })
    return deferred.promise
  }

  //aggiungo l'hash_opera al data_server

  const hash_opere = _.map(data_current_db, function(o) {
    return _.pick(o, ['hash_opera', 'identifier'])
  })

  data_server = _.map(data_server, function(obj) {
    return _.assign(obj, _.find(hash_opere, {
      identifier: obj.identifier
    }));
  })

  //by minor
  const by_minor = _.differenceBy(data_server, data_current_db, 'minor')

  //by major
  const by_major = _.differenceBy(data_server, data_current_db, 'major')

  if (by_minor.length <= 0 && by_major.length >= 1) {
    dove_cerco = by_major
  } else if (by_minor.length >= 1 && by_major.length <= 0) {
    dove_cerco = by_minor
  } else if (by_minor.length >= 1 && by_major.length >= 1) {
    //va bene uno dei due.
    dove_cerco = by_major
  }

  if (dove_cerco == false) {

    deferred.resolve({
      http: 200,
      errore: false,
      r: "Nessun beacon da aggiornare nel db"
    })
    return deferred.promise
  }

  _.map(dove_cerco, function(o) {
    $db.query("UPDATE beacons SET major = ?, minor = ? WHERE identifier = ?", [o.major, o.minor, o.identifier])
    $db.query("DELETE FROM dump_scaricati WHERE hash_opera = ?", [o.hash_opera])
  })

  deferred.resolve({
    http: 200,
    errore: false,
    r: "Beacon aggiornato nel db"
  })

  return deferred.promise
}

module.exports = {
  get_beacon,
  aggiorna_data_db
}