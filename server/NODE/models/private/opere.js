const db = require('../../helpers/db.js')
const hfile = require('../../helpers/file.js')
const response = require('../../config/responses/error.js')
const mime = require('mime-types')
const crypto = require("crypto")

const exists_opera = (hash) => {
  const deferred = $q.defer();

  $db.query('SELECT COUNT(id_opera) as esiste FROM opere WHERE ?', {
    hash: hash
  }, (error, results, fields) => {
    if (error) {
      console.log('[errore] model/opere', error)
      deferred.resolve(response.E_SERVER)
    } else {
      if (Object.assign({}, results[0]).esiste == 1) {
        deferred.resolve(true);
      } else {
        deferred.resolve(false);
      }
    }
  })

  return deferred.promise
}

const del_opera = (hash) => {
  const deferred = $q.defer()

  $db.query('DELETE FROM opere WHERE ?', {
    hash: hash
  }, (error, results, fields) => {
    if (error) {
      console.log('[errore] model/opere', error)
      return deferred.resolve(response.E_SERVER)
    } else {
      if (results.affectedRows == 0) {
        deferred.resolve(response.ERR_DEL_OPERA)
      } else {

        $db.query('DELETE FROM beacons WHERE ?', {
          hash_opera: hash
        })

        $db.query("DELETE FROM dump_scaricati WHERE hash_opera = ?", [hash])

        hfile.del_dir('./public/resource/' + hash)

        deferred.resolve({
          http: 200,
          errore: false,
          r: "Opera rimossa correttamente"
        })
      }
    }
  })

  return deferred.promise
}

const get_opere = () => {
  const deferred = $q.defer()

  $db.query("SELECT * FROM opere", (error, results, fields) => {
    if (error) {
      console.log('[errore] model/opere', error)
      deferred.resolve(response.E_SERVER)
    } else {
      deferred.resolve({
        http: 200,
        errore: false,
        r: results,
      })
    }
  })

  return deferred.promise
}


const new_opera = (payload) => {

  const deferred = $q.defer();

  if (payload.length <= 0) {
    deferred.resolve(response.E_SERVER)
  }

  const file = payload[0]
  var data = payload[1]

  const hash_generato = data.hash

  if (file.thumb != null) {
    data["thumb"] = "thumb." + crypto.randomBytes(30).toString('hex') + "." + mime.extension(file.thumb.type)
  } else {
    data["thumb"] = "null"
  }

  if (file.audio != null) {
    data["audio"] = "audio." + crypto.randomBytes(30).toString('hex') + "." + mime.extension(file.audio.type)
  } else {
    data["audio"] = "null"
  }

  //richiesto beacon.
  if (data.beacon != null) {
    data = [data.beacon, data.quadro, data.thumb, data.audio, hash_generato, data.minor, data.major]
  } else {
    data = [data.beacon, data.quadro, data.thumb, data.audio, hash_generato]
  }


  $db.query('INSERT INTO opere(id_beacon, titolo , thumb, audio, hash) VALUES(?, ?, ?, ?, ?, ?)', data, (error, results, fields) => {

    if (error) {
      console.log('[errore] model/opere', error)
      return deferred.resolve(response.E_SERVER)
    } else {

      $db.query('INSERT INTO beacons(identifier, major, minor, hash_opera) VALUES(?,?,?,?)', [
        data[0],
        data[6],
        data[5],
        data[4]
      ])

      if (data[2] != "null") {
        hfile.add_file('./public/resource/' + hash_generato, file.thumb, data[2])
      }

      if (data[3] != "null") {
        hfile.add_file('./public/resource/' + hash_generato, file.audio, data[3])
      }

      deferred.resolve({
        http: 200,
        errore: false,
        r: "Opera aggiunta correttamente"
      })

    }

  })

  return deferred.promise
}

const update_opera = (payload) => {

  const deferred = $q.defer();

  if (payload.length <= 0) {
    deferred.resolve(response.E_SERVER)
  }

  const file = payload[0]
  var data = payload[1]
  var aggiorna_audio = false
  var aggiorna_thumb = false

  if (file.thumb != null) {
    data["thumb"] = "thumb." + crypto.randomBytes(30).toString('hex') + "." + mime.extension(file.thumb.type)
    aggiorna_thumb = true
  }

  if (file.audio != null) {
    data["audio"] = "audio." + crypto.randomBytes(30).toString('hex') + "." + mime.extension(file.audio.type)
    aggiorna_audio = true
  }

  //richiesto beacon.
  if (data.beacon != null) {
    data = [data.beacon, data.quadro, data.thumb, data.audio, data.hash, data.minor, data.major]
  } else {
    data = [data.beacon, data.quadro, data.thumb, data.audio, data.hash]
  }

  $db.query('UPDATE opere SET id_beacon = ?, titolo = ?, thumb = ?, audio = ? WHERE hash = ?', data, (error, results, fields) => {

    if (error) {
      console.log('[errore] model/opere', error)
      return deferred.resolve(response.E_SERVER)
    } else {

      if (data[0] != null) {
        $db.query('UPDATE beacons SET identifier = ?, major = ?, minor = ? WHERE hash_opera = ? ', [
          data[0],
          data[6],
          data[5],
          data[4]
        ])
      }

      $db.query("DELETE FROM dump_scaricati WHERE hash_opera = ?", [data[4]])

      if (aggiorna_thumb) {
        hfile.add_file('./public/resource/' + data[4], file.thumb, data[2])
      }

      if (aggiorna_audio) {
        hfile.add_file('./public/resource/' + data[4], file.audio, data[3])
      }

      deferred.resolve({
        http: 200,
        errore: false,
        r: "Opera aggiornata correttamente"
      })

    }

  })

  return deferred.promise

}

module.exports = {
  exists_opera,
  new_opera,
  del_opera,
  get_opere,
  update_opera
}