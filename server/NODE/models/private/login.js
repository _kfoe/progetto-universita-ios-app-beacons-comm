const response = require('../../config/responses/error.js')
const jwt = require('../../helpers/jwt.js')
var bcrypt = require('../../helpers/bcrypt.js');

const login = (username, password) => {

  const deferred = $q.defer();
  $db.query('SELECT password FROM accessi_private WHERE is_bannato = 0 AND username = ?', [username], (error, results, fields) => {
    if (error) {
      console.log('[errore] model/login', error)
      deferred.resolve(response.E_SERVER)
    } else {
      if (results.length <= 0) {
        deferred.resolve(response.ERR_LOGIN)
      }

      results = Object.assign({}, results[0])

      if (!bcrypt.verifica(password, results.password)) {
        deferred.resolve(response.ERR_LOGIN)
      } else {

        deferred.resolve({
          http: 200,
          errore: false,
          r: jwt.genera("PRIVATE")
        })

      }
    }
  })
  return deferred.promise
}

module.exports = {
  login
}