-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Aug 30, 2017 at 01:44 PM
-- Server version: 5.6.35
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `projectunicam`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessi_app`
--

CREATE TABLE `accessi_app` (
  `id` int(11) NOT NULL,
  `codice` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_bannato` tinyint(1) NOT NULL DEFAULT '0',
  `ultimo_accesso` datetime NOT NULL,
  `last_uuid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accessi_app`
--

INSERT INTO `accessi_app` (`id`, `codice`, `password`, `is_bannato`, `ultimo_accesso`, `last_uuid`) VALUES
(1, '2303', '$2a$10$w5nVFrpyODD.PNQtEKRd4eJWZkdPau/5vN4qQ3jgANq80dwKt.Exq', 0, '2017-08-30 08:35:48', 'A0B18A78-0F5C-4B6B-982D-C56DB73F8533');

-- --------------------------------------------------------

--
-- Table structure for table `accessi_private`
--

CREATE TABLE `accessi_private` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_bannato` tinyint(1) NOT NULL DEFAULT '1',
  `ultimo_accesso` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accessi_private`
--

INSERT INTO `accessi_private` (`id`, `username`, `password`, `is_bannato`, `ultimo_accesso`) VALUES
(1, 'k', '$2a$10$w5nVFrpyODD.PNQtEKRd4eJWZkdPau/5vN4qQ3jgANq80dwKt.Exq', 0, '2017-08-29 07:59:40');

-- --------------------------------------------------------

--
-- Table structure for table `beacons`
--

CREATE TABLE `beacons` (
  `id_collegamento` int(11) NOT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `hash_opera` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `beacons`
--

INSERT INTO `beacons` (`id_collegamento`, `identifier`, `major`, `minor`, `hash_opera`) VALUES
(55, 'c59de60678fa', 30970, 58886, '26cdf82a93a4fda5713b9ec5975358a7553c352f'),
(56, 'd3e5a4437c11', 31761, 42051, '0c87b9174147ca856fb74efef5480ecfa0f31b3e');

-- --------------------------------------------------------

--
-- Table structure for table `dump_scaricati`
--

CREATE TABLE `dump_scaricati` (
  `id_download` bigint(20) NOT NULL,
  `hash_opera` varchar(255) NOT NULL,
  `uuid_iphone` varchar(255) NOT NULL,
  `scaricato_il` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dump_scaricati`
--

INSERT INTO `dump_scaricati` (`id_download`, `hash_opera`, `uuid_iphone`, `scaricato_il`) VALUES
(24, '26cdf82a93a4fda5713b9ec5975358a7553c352f', 'A0B18A78-0F5C-4B6B-982D-C56DB73F8533', '2017-08-30 13:27:32'),
(25, '0c87b9174147ca856fb74efef5480ecfa0f31b3e', 'A0B18A78-0F5C-4B6B-982D-C56DB73F8533', '2017-08-30 13:43:57');

-- --------------------------------------------------------

--
-- Table structure for table `opere`
--

CREATE TABLE `opere` (
  `id_opera` int(11) NOT NULL,
  `id_beacon` varchar(255) DEFAULT 'null',
  `titolo` varchar(200) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `audio` varchar(255) DEFAULT 'null',
  `hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opere`
--

INSERT INTO `opere` (`id_opera`, `id_beacon`, `titolo`, `thumb`, `audio`, `hash`) VALUES
(126, 'c59de60678fa', 'test test', 'thumb.971b8570c2c827bff0cc72a777d3bfcdb00c90592f7b0cb01a381ad72706.jpeg', 'audio.ba56c3504d6a836ba175991136206642151ec6f85d905dba86426e651ce2.mp3', '26cdf82a93a4fda5713b9ec5975358a7553c352f'),
(127, 'd3e5a4437c11', 'test test test', 'thumb.78629b206b5dbe8d4dfd44889e65b4cec7e744505f41143a8c7a60e1d63d.jpeg', 'audio.b4a149f0dbc04209341757c8e7bb465e43cb158bea13dfe8a9a5dfd1e18e.mp3', '0c87b9174147ca856fb74efef5480ecfa0f31b3e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessi_app`
--
ALTER TABLE `accessi_app`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accessi_private`
--
ALTER TABLE `accessi_private`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `beacons`
--
ALTER TABLE `beacons`
  ADD PRIMARY KEY (`id_collegamento`),
  ADD UNIQUE KEY `identifier_2` (`identifier`),
  ADD KEY `identifier` (`identifier`),
  ADD KEY `hash_opera` (`hash_opera`);

--
-- Indexes for table `dump_scaricati`
--
ALTER TABLE `dump_scaricati`
  ADD PRIMARY KEY (`id_download`),
  ADD UNIQUE KEY `hash_opera` (`hash_opera`);

--
-- Indexes for table `opere`
--
ALTER TABLE `opere`
  ADD PRIMARY KEY (`id_opera`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `id_beacon` (`id_beacon`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessi_app`
--
ALTER TABLE `accessi_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `accessi_private`
--
ALTER TABLE `accessi_private`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `beacons`
--
ALTER TABLE `beacons`
  MODIFY `id_collegamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `dump_scaricati`
--
ALTER TABLE `dump_scaricati`
  MODIFY `id_download` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `opere`
--
ALTER TABLE `opere`
  MODIFY `id_opera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `beacons`
--
ALTER TABLE `beacons`
  ADD CONSTRAINT `beacons_ibfk_1` FOREIGN KEY (`identifier`) REFERENCES `opere` (`id_beacon`) ON DELETE CASCADE ON UPDATE CASCADE;
