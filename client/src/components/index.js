import Vue from 'vue'

Vue.component('app-header', require('./header/Header.vue'))
Vue.component('app-footer', require('./footer/Footer.vue'))
Vue.component('app-search', require('./search/search.vue'))

