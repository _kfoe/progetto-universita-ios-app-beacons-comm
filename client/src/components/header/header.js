import {
  mapState
} from 'vuex'
import * as helpers from '../../helpers.js'
export default {
  computed: mapState(['header', 'modale_associazione']),
  methods: {
    apri_modale_associazione: function() {
      const t = this.$store.state.modale_associazione

      t.attiva = true
      t.is_aggiornamento = false
      
      t.audio = null
      t.beacon = null
      t.descrizione = null
      t.quadro = null
      t.hash = null
      t.thumb = null

    },
    termina_sessione: function() {
      helpers.set("TOKEN", null)
      this.$store.state.is_connesso = false
      window.location.href = "./";
    },
    is_connesso: function() {
      if (helpers.get("TOKEN") == null || helpers.get("TOKEN") == "null") {
        return false
      }
      return true
    }
  }
}