const audio = ['audio/mpeg3', 'audio/x-mpeg-3', 'audio/mpeg', 'audio/mp3']
const image = ['image/jpeg', 'image/pjpeg', 'image/png']

export {
	audio,
	image
}