import {
  mapState,
  mapActions,
  mapGetters
} from 'vuex'
import * as helpers from '../../helpers.js'
import * as allow from './format.js'
import _ from 'lodash'
export default {
  data: function() {
    return {
      modale_attiva: false
    }
  },
  mounted: function() {
    this.$nextTick(function() {
      if (helpers.get('TOKEN') == null || helpers.get("TOKEN") == "null") {
        window.location.href = "./";
        return
      }

      this.$store.state.header.pagina = "Lista associazioni"
      this.$store.state.header.descrizione = "Opere e Beacons"

      //stampa bottone inserisci nuova opera
      this.$store.state.modale_associazione.can = true

      this.$store.dispatch('ESTIMOTE_get_beacons')
      this.$store.dispatch('SERVER_get_opere')

      setTimeout(() => {
        this.$store.dispatch('SERVER_get_beacons')
      }, 1000)

      setTimeout(() => {
        this.$store.dispatch('SERVER_aggiorna_beacons_db')
      }, 2000)

    })
  },
  methods: {
    is_attivo: function(id_beacon) {
      if (id_beacon == null || id_beacon == undefined || id_beacon == "null" || id_beacon == "") {
        return "non_attivo"
      }
      return "attivo"
    },
    get_path: function(hash, image) {
      if (image && hash) {
        return "http://localhost:4113/resource/" + hash + "/" + image
      }
      return "http://localhost:4113/resource/no-thumb.jpg"
    },
    get_path_aggiorna: function(hash, data) {
      return "http://localhost:4113/resource/" + hash + "/" + data
    },
    elimina_opera: function(hash) {
      this.$store.dispatch('SERVER_erase_opera', hash)
    },
    associa: function(check) {
      const th = this.$store.state.modale_associazione

      if (th.quadro == '' || th.thumb == '' || th.thumb == null) {
        helpers.alert(this.$store.state, 'Nome, descrizione e immagine sono obbligatori')
        return false;
      }

      const f = new FormData();

      //Se ho selezionato un beacon dalla lista prelevo i dati aggiornati 
      if ((this.$store.state.server.beacons_disponibili.selezionato != null && this.$store.state.server.beacons_disponibili.selezionato != "null" && this.$store.state.server.beacons_disponibili.selezionato != undefined) ||
        (this.$store.state.server.beacons_associati_piu_disponibili.selezionato != null && this.$store.state.server.beacons_associati_piu_disponibili.selezionato != "null" && this.$store.state.server.beacons_associati_piu_disponibili.selezionato != undefined)) {

        var cerca_in = ""

        if (this.$store.state.modale_associazione.is_aggiornamento == true) {
          cerca_in = this.$store.state.server.beacons_associati_piu_disponibili
        } else {
          cerca_in = this.$store.state.server.beacons_disponibili
        }

        const current_beacon = _.find(cerca_in.options, {
          identifier: cerca_in.selezionato
        })

        f.append('minor', current_beacon.settings.advertisers.ibeacon[0].minor)
        f.append('major', current_beacon.settings.advertisers.ibeacon[0].major)

      }

      f.append('quadro', th.quadro)

      if (check == true) {
        f.append('beacon', this.$store.state.server.beacons_disponibili.selezionato)
      } else {
        f.append('beacon', this.$store.state.server.beacons_associati_piu_disponibili.selezionato)
      }

      f.append('audio', th.audio)
      f.append('thumb', th.thumb)

      //check true = nuova ssociazione
      if (check == true) {
        this.$store.dispatch('SERVER_nuova_associazione', f)
      } else {
        f.append('hash', check)
        this.$store.dispatch('SERVER_aggiorna_associazione', f)
        //altrimenti sto chiedendo un'update e check contiene l'hash dell'opera

      }

    },
    aggiorna_associazione: function(hash) {
      const th = this.$store.state
      th.modale_associazione.is_aggiornamento = true

      const sete = [...new Set([].concat(...th.server.opere))]

      const current = _.find(sete, {
        hash: hash
      })

      th.modale_associazione.audio = current.audio
      th.modale_associazione.quadro = current.titolo
      th.modale_associazione.thumb = current.thumb
      th.modale_associazione.hash = hash
      th.modale_associazione.attiva = true

      if (th.modale_associazione.is_aggiornamento == true) {
        th.server.beacons_associati_piu_disponibili.selezionato = current.id_beacon
      } else {
        th.server.beacons_disponibili.selezionato = current.id_beacon
      }

    },
    upload_audio(e) {

      var files = e.target.files || e.dataTransfer.files;
      if (!files.length)
        return false

      if (!helpers.is_allow_type(allow.audio, files)) {
        helpers.alert(this.$store.state, 'Formato audio non consentito')
        this.$store.state.modale_associazione.audio = null
        return false;
      }

      this.$store.state.modale_associazione.audio = files[0]

    },
    upload_thumb(e) {
      var files = e.target.files || e.dataTransfer.files;
      if (!files.length)
        return false

      if (!helpers.is_allow_type(allow.image, files)) {
        helpers.alert(this.$store.state, 'Formato immagine non consentito')
        this.$store.state.modale_associazione.thumb = null
        return false
      }

      this.$store.state.modale_associazione.thumb = files[0]
    },
    chiudi_modale: function() {
      this.$store.state.modale_associazione.attiva = false
    },
    aggiorna_thumb: function(){
      this.$refs.thumb.click()
    }
  },
  computed: mapState(['header', 'modale_associazione', 'server'])
}