import {
  mapState,
  mapActions,
  mapGetters
} from 'vuex'
export default {
  data: function() {
    return {
      username: '',
      password: ''
    }
  },
  mounted: function() {
    this.$nextTick(function() {
      this.$store.state.header.pagina = "Area riservata"

            this.$store.state.modale_associazione.can = false
    })
  },
  methods: mapActions(['SERVER_login']),
  computed: mapState(['header'])
}