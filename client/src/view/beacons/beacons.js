import {
  mapState,
  mapActions,
  mapGetters
} from 'vuex'
import * as helpers from '../../helpers.js'
export default {
  mounted: function() {
    this.$nextTick(function() {
      console.log(helpers.get('TOKEN'))
      if (helpers.get('TOKEN') == null || helpers.get("TOKEN") == "null" || helpers.get("TOKEN") == "") {
        window.location.href = "./";
        return
      }

      this.$store.state.header.pagina = "Lista Beacons Estimote"
      this.$store.state.header.descrizione = "Gestisci i beacons registrati su Estimote cloud"
      this.$store.state.modale_associazione.can = false

      this.$store.dispatch('ESTIMOTE_get_beacons', true)
    })
  },
  computed: mapState(['estimote', 'modale_aggiorna_beacons']),
  methods: {
    get_immagine: (colore) => {
      return "https://d3j8zbxkez4k99.cloudfront.net/images/beacons/" + colore + ".png"
    },
    chiudi_modale: function() {
      document.body.style.overflow = 'visible';
      this.$store.state.modale_aggiorna_beacons.attiva = false
    },
    aggiorna_beacons_modale: function(id, nome, pending_settings, others) {
      document.body.style.overflow = 'hidden';

      const th = this.$store.state.modale_aggiorna_beacons

      th.titolo_beacon_in_aggiornamento = nome
      th.enabled.selezionato = others.enabled
      th.identifier = id
      th.nome_beacon = nome
      th.major = others.major
      th.minor = others.minor
      th.power.selezionato = String(others.power)
      th.interval = others.interval
      th.pending = pending_settings
      th.index = others.index
      th.uuid = others.uuid
      th.non_strict_mode_enabled = others.non_strict_mode_enabled

      th.attiva = true
    },
    aggiorna_beacons_actions: function(data){
      this.$store.dispatch("ESTIMOTE_aggiorna_beacon", data)
    }
  }
}