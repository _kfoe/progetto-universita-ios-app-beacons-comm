const select_enabled = {
  selezionato: true,
  options: [{
    value: true,
    name: "Sì"
  }, {
    value: false,
    name: "No"
  }]
}

export {
  select_enabled
}