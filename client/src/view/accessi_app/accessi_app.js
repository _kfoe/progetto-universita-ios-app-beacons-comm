import {
  mapState,
  mapActions,
  mapGetters
} from 'vuex'
import * as helpers from '../../helpers.js'
export default {
  mounted: function() {
    this.$nextTick(function() {

      if (helpers.get('TOKEN') == null || helpers.get("TOKEN") == "null") {
        window.location.href = "./";
        return
      }

      this.$store.state.header.pagina = "Postazioni registrate"
      this.$store.state.header.descrizione = "Gestici le postazioni che possono usare l'applicazione"
      this.$store.state.modale_associazione.can = false
      this.$store.dispatch("SERVER_get_postazioni_app")
    })
  },
  methods: mapActions(['SERVER_aggiorna_postazione_app', 'SERVER_cancella_postazione_app']),
  computed: mapState(['server'])
}