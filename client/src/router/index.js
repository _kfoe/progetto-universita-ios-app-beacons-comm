import Vue from 'vue'
import VueRouter from 'vue-router'
import R from './routes.js'

Vue.use(VueRouter)

const router = new VueRouter({
 mode: 'history',
 base: __dirname,
 routes: R.routes
})

export default router