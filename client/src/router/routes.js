import Login from '../view/login/login.vue'
import Homepage from '../view/homepage/Homepage.vue'
import Accessi_sito from '../view/accessi_sito/accessi_sito.vue'
import Accessi_app from '../view/accessi_app/accessi_app.vue'
import Beacons from '../view/beacons/beacons.vue'

export default {
  routes: [{
    path: '/',
    name: 'login',
    component: Login,
  }, {
    path: '/lista',
    name: 'homepage',
    component: Homepage
  }, {
    path: '/accessi_sito',
    name: 'accessi_sito',
    component: Accessi_sito
  }, {
    path: '/beacons',
    name: 'beacons',
    component: Beacons
  }, {
    path: '/accessi_app',
    name: 'accessi_app',
    component: Accessi_app
  }]
}