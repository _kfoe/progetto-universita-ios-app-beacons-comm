import Request from 'axios'
import _ from 'lodash'
import Vueinstance from '../../../main.js'
import * as helpers from '../../../helpers.js'

var ENDPOINT = {
  estimote: "https://cloud.estimote.com"
}

const ESTIMOTE = Request.create({
  baseURL: ENDPOINT.estimote,
  auth: {
    username: 'beacons-demo-test-2jb',
    password: '400270b7975015c1aafc7128e1da66e9'
  }
})

ESTIMOTE.defaults.headers.common['Content-Type'] = "application/json";


const ESTIMOTE_get_beacons = ({
  state,
  commit
}, flag = false) => {
  ESTIMOTE.get('/v2/devices')
    .then(function(response) {

      if (flag == true) {
        commit('ESTIMOTE_beacons_no_filtro', response.data)
      } else {
        commit('ESTIMOTE_beacons', response.data)
      }
    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const ESTIMOTE_aggiorna_beacon = ({
  state,
  commit
}, data) => {

  const payload = {
    "shadow": {
      "name": data.nome_beacon
    },
    "pending_settings": {
      "advertisers": {
        "ibeacon": [{
          "index": data.index,
          "uuid": data.uuid,
          "non_strict_mode_enabled": data.non_strict_mode_enabled,
          "enabled": data.enabled.selezionato,
          "major": parseInt(data.major),
          "minor": parseInt(data.minor),
          "power": parseInt(data.power.selezionato),
          "interval": parseInt(data.interval),
        }]
      }
    }
  }

  ESTIMOTE.post('https://cloud.estimote.com/v2/devices/' + data.identifier, payload)
    .then(function(response) {
      if (response.data.success == true) {
        helpers.alert(state, "Beacon aggiornato correttamente!")

        setTimeout(() => {
          location.reload()
        }, 2000)
      } else {
        helpers.alert(state, "Errore durante l\'aggiornamento del beacon")
        return false
      }
    }).catch((e) => {
      helpers.alert(state, "Impossibile aggiornare questo beacon!")
      console.log(e)
    })

}

export {
  ESTIMOTE_get_beacons,
  ESTIMOTE_aggiorna_beacon
}