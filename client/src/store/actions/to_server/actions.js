import Request from 'axios'
import _ from 'lodash'
import Vueinstance from '../../../main.js'
import * as helpers from '../../../helpers.js'

var ENDPOINT = {
  server: "http://localhost:4113"
}

const SERVER_NOAUTH = Request.create({
  baseURL: ENDPOINT.server,
})
SERVER_NOAUTH.defaults.headers.common['Content-Type'] = "application/json";

const SERVER_AUTH = Request.create({
  baseURL: ENDPOINT.server,
  headers: {
    Authorization: 'Bearer ' + helpers.get('TOKEN')
  }
})


const SERVER_login = ({
  commit,
  state
}, data) => {

  data = {
    username: data[0],
    password: data[1]
  };

  SERVER_NOAUTH.post('/private/login', data)
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      helpers.set('TOKEN', response.data.r)

      setTimeout(() => {
        helpers.redirect(Vueinstance, '/lista')
        location.reload()
      }, 2000)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
      return false
    })
}

const SERVER_get_beacons = ({
  commit,
  state
}) => {
  SERVER_AUTH.get('/private/beacons')
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      commit('SERVER_beacons', response.data.r)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_get_opere = ({
  commit,
  state
}) => {
  SERVER_AUTH.get('/private/opere')
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      commit('SERVER_opere', response.data.r)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_erase_opera = ({
  commit,
  state
}, hash) => {
  SERVER_AUTH.delete('/private/opera/' + hash, {
      params: {
        hash: hash
      }
    })
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      commit('SERVER_ERASE_opera', hash)
    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_nuova_associazione = ({
  commit,
  state
}, data) => {
  SERVER_AUTH.post('/private/opera/nuova', data)
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      state.modale_associazione.attiva = false

      helpers.alert(state, response.data.r)

      setTimeout(() => {
        location.reload()
      }, 2200)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_aggiorna_associazione = ({
  commit,
  state
}, data) => {
  SERVER_AUTH.put('/private/opera/aggiorna', data)
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      state.modale_associazione.attiva = false

      helpers.alert(state, response.data.r)

      setTimeout(() => {
        location.reload()
      }, 2200)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_get_postazioni_private = ({
  commit,
  state
}) => {
  SERVER_AUTH.get("/private/accessi_sito")
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      commit("AGGIORNA_POSTAZIONI_PRIVATE", response.data.r)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_aggiorna_postazione_private = ({
  commit,
  state
}, data) => {
  SERVER_AUTH.put("/private/accessi_sito/aggiorna", {
      utente: data[0],
      flag: data[1]
    })
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      helpers.alert(state, response.data.r)

      setTimeout(() => {
        location.reload()
      }, 2000)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_cancella_postazione_private = ({
  commit,
  state
}, utente) => {
  SERVER_AUTH.delete("/private/accessi_sito/cancella", {
      params: {
        utente: utente
      }
    })
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      helpers.alert(state, response.data.r)

      setTimeout(() => {
        location.reload()
      }, 2000)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_get_postazioni_app = ({
  commit,
  state
}) => {
  SERVER_AUTH.get("/private/accessi_app")
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      commit("AGGIORNA_POSTAZIONI_APP", response.data.r)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_aggiorna_postazione_app = ({
  commit,
  state
}, data) => {
  SERVER_AUTH.put("/private/accessi_app/aggiorna", {
      id: data[0],
      flag: data[1]
    })
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      helpers.alert(state, response.data.r)

      setTimeout(() => {
        location.reload()
      }, 2000)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_cancella_postazione_app = ({
  commit,
  state
}, codice) => {
  SERVER_AUTH.delete("/private/accessi_app/cancella", {
      params: {
        id: codice
      }
    })
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

      helpers.alert(state, response.data.r)

      setTimeout(() => {
        location.reload()
      }, 2000)

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

const SERVER_aggiorna_beacons_db = ({
  commit,
  state
}) => {

  var payload = []

  const a = _.map(state.server.beacons_associati, function(o) {
    return _.pick(o, ['identifier', 'settings.advertisers.ibeacon[0].minor', 'settings.advertisers.ibeacon[0].major'])
  })

  _.map(a, function(o) {
    payload.push({
      identifier: o.identifier,
      minor: o.settings.advertisers.ibeacon[0].minor,
      major: o.settings.advertisers.ibeacon[0].major
    })
  })

  SERVER_AUTH.post("/private/beacons/db", payload)
    .then((response) => {

      if (response.data.errore == true) {
        helpers.alert(state, response.data.r)
        return false
      }

    }).catch((e) => {
      helpers.alert(state, "Errore durante la richiesta")
      console.log(e)
    })
}

export {
  SERVER_login,
  SERVER_get_beacons,
  SERVER_get_opere,
  SERVER_erase_opera,
  SERVER_nuova_associazione,
  SERVER_aggiorna_associazione,
  SERVER_get_postazioni_private,
  SERVER_aggiorna_postazione_private,
  SERVER_cancella_postazione_private,
  SERVER_get_postazioni_app,
  SERVER_aggiorna_postazione_app,
  SERVER_cancella_postazione_app,
  SERVER_aggiorna_beacons_db
}