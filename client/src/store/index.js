import Vue from 'vue'
import Vuex from 'vuex'

//import * as actions from './actions/actions/to_estimote'
import * as getters from './getters'
import * as actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const defaultState = {
  footer: {
    avviso: false,
    messaggio: ''
  },
  header: {
    pagina: '',
    descrizione: ''
  },
  modale_associazione: {
    attiva: false,
    is_aggiornamento: false,
    quadro: '',
    thumb: null,
    audio: null,
    beacon: false,
    hash: false,
    can: false
  },
  modale_aggiorna_beacons: {
    attiva: false,
    titolo_beacon_in_aggiornamento: false,
    index: 0,
    uuid: 0,
    non_strict_mode_enabled: false,
    pending: false,
    enabled: {
      selezionato: true,
      options: [{
        value: true,
        name: "Sì"
      }, {
        value: false,
        name: "No"
      }]
    },
    identifier: 0,
    nome_beacon: '',
    major: 0,
    minor: 0,
    power: {
      selezionato: "-30",
      options: [{
        value: -40,
        name: "-40 (SOLO UWB)"
      }, {
        value: -30,
        name: "-30"
      }, {
        value: -20,
        name: "-20"
      }, {
        value: -16,
        name: "-16"
      }, {
        value: -12,
        name: "-12"
      }, {
        value: -8,
        name: "-8"
      }, {
        value: -4,
        name: "-4"
      }, {
        value: 0,
        name: "0"
      }, {
        value: 4,
        name: "4"
      }, {
        value: 20,
        name: "20 (SOLO UWB)"
      }]
    },
    interval: 0
  },
  estimote: {
    beacons: []
  },
  server: {
    beacons_associati: [],
    beacons_disponibili: [],
    beacons_associati_piu_disponibili: [],
    opere: [],
    postazioni_private: [],
    postazioni_app: []
  }
}

const inBrowser = typeof window !== 'undefined'

const state = (inBrowser && window.__INITIAL_STATE__) || defaultState

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})