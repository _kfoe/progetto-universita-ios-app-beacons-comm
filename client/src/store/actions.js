import {
  ESTIMOTE_get_beacons,
  ESTIMOTE_aggiorna_beacon
} from './actions/to_estimote/actions'

import {
  SERVER_login,
  SERVER_get_beacons,
  SERVER_get_opere,
  SERVER_erase_opera,
  SERVER_nuova_associazione,
  SERVER_aggiorna_associazione,
  SERVER_get_postazioni_private,
  SERVER_aggiorna_postazione_private,
  SERVER_cancella_postazione_private,
  SERVER_get_postazioni_app,
  SERVER_aggiorna_postazione_app,
  SERVER_cancella_postazione_app,
  SERVER_aggiorna_beacons_db
} from './actions/to_server/actions'

export {
  SERVER_login,
  SERVER_get_beacons,
  SERVER_get_opere,
  SERVER_erase_opera,
  SERVER_nuova_associazione,
  SERVER_aggiorna_associazione,
  SERVER_get_postazioni_private,
  SERVER_aggiorna_postazione_private,
  SERVER_cancella_postazione_private,
  SERVER_get_postazioni_app,
  SERVER_aggiorna_postazione_app,
  SERVER_cancella_postazione_app,
  SERVER_aggiorna_beacons_db,
  ESTIMOTE_get_beacons,
  ESTIMOTE_aggiorna_beacon
}

//solve preflight https://stackoverflow.com/questions/35588699/response-to-preflight-request-doesnt-pass-access-control-check