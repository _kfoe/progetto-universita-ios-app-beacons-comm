import _ from 'lodash'

export default {
  ESTIMOTE_beacons: (state, data) => {
    state.estimote.beacons = _.map(_.filter(data, function(o) {
      return o.settings.advertisers.ibeacon[0].enabled;
    }), function(o) {
      return _.pick(o, ['identifier', 'color', 'settings.advertisers.ibeacon[0]', 'shadow.name'])
    })
  },
  ESTIMOTE_beacons_no_filtro: (state, data) => {
    state.estimote.beacons = _.map(data, function(o) {
      return _.pick(o, ['identifier', 'color', 'settings.advertisers.ibeacon[0]', 'shadow.name', 'pending_settings'])
    })
  },
  SERVER_beacons: (state, data) => {

    const associati = _.map(data, function(obj) {
      return _.assign(obj, _.find(state.estimote.beacons, {
        identifier: obj.identifier
      }));
    });

    const disponibili = _.differenceBy(state.estimote.beacons, associati, 'identifier')

    const associati_piu_usabili = _.map(state.estimote.beacons, function(obj) {
      return _.assign(obj, _.find(associati, {
        identifier: obj.identifier
      }));
    })

    associati_piu_usabili.push({
      identifier: null,
      shadow: {
        name: 'Non associato'
      },
    })

    if (disponibili.length <= 0) {
      state.server.beacons_disponibili = {
        selezionato: null,
        options: [{
          identifier: null,
          shadow: {
            name: 'Nessun beacon disponibile'
          }
        }]
      }
    } else {
      disponibili.push({
        identifier: null,
        shadow: {
          name: 'Non associato'
        },
      })
      state.server.beacons_disponibili = {
        selezionato: null,
        options: disponibili,
      }
    }

    state.server.beacons_associati_piu_disponibili = {
      selezionato: associati_piu_usabili[0]["identifier"],
      options: associati_piu_usabili
    }

    state.server.beacons_associati = associati

  },
  SERVER_opere: (state, data) => {
    state.server.opere = _.chunk(data, 3);
  },
  SERVER_ERASE_opera: (state, hash) => {

    const sete = [...new Set([].concat(...state.server.opere))]
    state.server.opere = _.chunk(_.reject(sete, {
      hash: hash
    }), 3)
  },
  AGGIORNA_POSTAZIONI_PRIVATE: (state, data) => {
    state.server.postazioni_private = data
  },
  AGGIORNA_POSTAZIONI_APP: (state, data) => {
    state.server.postazioni_app = data
  }
}

/*
export default {
 SET_BEACONS_ESTIMOTE: function(state, beacons) {
  //filtro disattivato 
   state.lista_beacons_ESTIMOTE = _.filter(beacons, function(o) {
   return o.settings.advertisers.ibeacon[0].enabled;
  });

  state.lista_beacons_ESTIMOTE = beacons
 },
 SET_BEACONS_SERVER: function(state, beacons) {
  //filtro disattivato 
  state.lista_beacons_ESTIMOTE = _.filter(beacons, function(o) {
   return o.settings.advertisers.ibeacon[0].enabled;
  });

  state.lista_beacons_SERVER = beacons
 },
 SET_BEACONS_MERGED: function(state) {
  //effettuo il merge.
   state.lista_beacon_merged = _.map(state.lista_beacons_ESTIMOTE, function(obj) {
    return _.assign(obj, _.find(state.lista_beacons_SERVER, {
     identifier: obj.identifier
    }));
   });

 },
 AGGIORNA_TOKEN: function(state, token) {
  state.token = token
 },
 AGGIORNA_DATA_TEMP: function(state, data) {
  state.data_temp = data
 },
 RESET_data_temp: function(state) {
  state.data_temp = []
 },
 GET_DATA_FOR_ID: function(state, id_beacon) {
  state.data_temp = _.find(state.lista_beacon_merged, {
   identifier: id_beacon
  })
 }
}
*/