import _ from 'lodash'

const alert = (state, messaggio) => {
  state.footer.avviso = true;
  state.footer.messaggio = messaggio

  setTimeout(() => {
    state.footer.avviso = false
  }, 2000)
}

const set = (key, data) => {
  sessionStorage.setItem(key, data)
}

const get = (key) => {
  return sessionStorage.getItem(key)
}

const redirect = (vueinstance, route) => {
  vueinstance.$router.push(route)
}

const is_allow_type = (types, file) => {

  if (types.length <= 0) {
    return false
  }

  if (_.includes(types, file[0].type)) {
    return true
  }
  return false
}

export {
  alert,
  set,
  get,
  redirect,
  is_allow_type
}