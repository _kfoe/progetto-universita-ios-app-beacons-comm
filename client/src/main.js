import Vue from 'vue'
import router from './router'
import Base from './Base.vue'
import store from './store'
import Components from './components'
import filter from './filter'
import directive from './directive'
import {
  sync
} from 'vuex-router-sync'
import pace from 'pace'

import style from './assets/css/style.scss'
import core from './assets/js/core.js'
import milligram from 'milligram'

sync(store, router)

var x = new Vue({
  router,
  store,
  el: '#app',
  render: h => h(Base)
})

export default x