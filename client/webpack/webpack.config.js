var Path = require('path');
var webpack = require('webpack');
var utils = require('./utils');
const BASE = Path.resolve(__dirname, './../');

require('dotenv').config({
 path: Path.join(BASE, './.env')
});

module.exports = {
 module: {
  rules: [{
   test: /\.js$/,
   use: ['babel-loader']
  }, {
   test: /\.vue$/,
   use: [{
    loader: 'vue-loader'
   }]
  }, {
   test: /\.(jpg|png|jpeg|gif)$/,
   use: 'file-loader?name=images/[hash].[ext]'
  }, {
   test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
   use: 'url-loader?limit=10000&mimetype=application/font-woff&name=fonts/[hash].[ext]'
  }, {
   test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
   use: 'url-loader?limit=10000&mimetype=application/font-woff&name=fonts/[hash].[ext]'
  }, {
   test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
   use: 'url-loader?limit=10000&mimetype=application/octet-stream&name=fonts/[hash].[ext]'
  }, {
   test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
   use: 'file-loader?name=fonts/[hash].[ext]'
  }, {
   test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
   use: 'url-loader?limit=10000&mimetype=image/svg+xml&name=fonts/[hash].[ext]'
  }]
 },
 resolve: {
  modules: ["bower", "node_modules"],
  alias: utils.resolveBower({
   bowerPath: Path.join(BASE, '/bower'),
   exclude: [],
   showInfo: true,
   manualResolve: {}
  })
 }
}