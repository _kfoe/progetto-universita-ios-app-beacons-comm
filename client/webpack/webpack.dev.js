var Path = require('path');
var Webpack = require('webpack');
var Merge = require('webpack-merge');
var Clean = require('clean-webpack-plugin');
var Copy = require('copy-webpack-plugin');
var Extract = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
var BowerWebpackPlugin = require("bower-webpack-plugin");
var Html = require('html-webpack-plugin');
const CONFIG = require('./webpack.config.js');
const BASE = Path.resolve(__dirname, './../');

module.exports = Merge(CONFIG, {
 devtool: 'eval',
 entry: Path.join(BASE, '/src/main.js'),
 output: {
  publicPath: 'http://localhost:8080/',
  path: Path.join(BASE, '/dev'),
  filename: 'js/[hash].js'
 },
 devServer: {
  historyApiFallback: true,
  noInfo: false,
  inline: true,
  hot: true
 },
 resolve: {
  alias: {
   'vue': 'vue/dist/vue.js',
   'bg_login': './../src/assets/images/bg.jpg'
  }
 },
 module: {
  rules: [{
   test: /\.css$/,
   use: [{
    loader: 'style-loader'
   }, {
    loader: 'css-loader?sourceMap',
    options: {
     modules: false
     //localIdentName: "[name]__[hash:base64:5]",
    }
   }]
  }, {
   test: /\.scss$/,
   use: [{
    loader: 'style-loader'
   }, {
    loader: 'css-loader?sourceMap',
    options: {
     modules: false,
     //localIdentName: "[name]__[hash:base64:5]"
    }
   }, {
    loader: 'autoprefixer-loader'
   }, {
    loader: 'sass-loader'
   }]
  }]
 },
 plugins: [
  new Clean(['dev/css', 'dev/js', 'dev/fonts'], {
   root: BASE,
   verbose: true,
   dry: false,
   exclude: []
  }),
  new Webpack.HotModuleReplacementPlugin(),
  new Webpack.optimize.OccurrenceOrderPlugin(),
  new Webpack.NoEmitOnErrorsPlugin(),
  new Html({
   filename: 'index.html',
   template: 'aindex.html'
  })
 ]
})