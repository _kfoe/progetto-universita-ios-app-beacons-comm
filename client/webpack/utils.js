var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var walkSync = require('walk-sync');


var alias = {};
exports.resolveBower = function(options) {
 var temp;

 function name(str) {
  return str.substr(0, str.indexOf('/')).toLowerCase().replace(/[^a-zA-Z0-9]/g, '');
 };

 temp = walkSync(options.bowerPath, {
  globs: ['**/.bower.json'],
  directories: false,
  ignore: options.exclude
 });
 temp.forEach(function(v) {
  temp = JSON.parse(fs.readFileSync(path.join(options.bowerPath, v), 'utf8'));
  if (_.isArray(temp.main)) {
   temp.main.forEach(function(vv) {
    var stringaRovesciata = vv.split("").reverse().join('');
    var estensione = stringaRovesciata.substr(0, stringaRovesciata.indexOf('.'));
    estensione = estensione.split("").reverse().join("");
    alias[name(v) + estensione] = path.join(options.bowerPath, temp.name + '/' + vv);
   });
  } else {
   alias[name(v)] = path.join(options.bowerPath, v.substr(0, v.indexOf('/')) + '/' + temp.main)
  }
 });
 _.merge(alias, options.manualResolve);

 if (options.showInfo) {
  for (x in alias) {
   console.warn('[bowerResolve] Alias: ' + x + ' ===> ' + alias[x]);
  }
  console.log('\n');
 }
 return alias;
};