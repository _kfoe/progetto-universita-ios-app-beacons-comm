//prima di eseguirlo va riscritto secondo la nuova forma webpack 3 guarda webpack.dev.js

var Path = require('path');
var Webpack = require('webpack');
var Merge = require('webpack-merge');
var Clean = require('clean-webpack-plugin');
var Extract = require('extract-text-webpack-plugin');
var BowerWebpackPlugin = require("bower-webpack-plugin");
var Html = require('html-webpack-plugin');
const CONFIG = require('./webpack.config.js');
const BASE = Path.resolve(__dirname, './../');

require('dotenv').config({
 path: Path.join(BASE, './.env')
});

module.exports = Merge(CONFIG, {
 devtool: false,
 entry: [
  Path.join(BASE, '/src/main.js')
 ],
 output: {
  publicPath: process.env.BASE_URL,
  path: Path.join(BASE, '/public'),
  filename: 'js/core.js'
 },
 module: {
  rules: [{
   test: /\.css$/,
   use: Extract.extract("style-loader", "css-loader?sourceMap&modules=false&localIdentName=[name]__[hash:base64:5]")
  }, {
   test: /\.scss$/,
   use: Extract.extract("style-loader", "css-loader?sourceMap&modules=false&localIdentName=[name]__[hash:base64:5]!autoprefixer-loader!sass-loader")
  }]
 },
 resolve: {
  alias: {
   'vue': 'vue/dist/vue.min.js',
  }
 },
 plugins: [
  new Clean(['public/css', 'public/js', 'public/fonts'], {
   root: BASE,
   verbose: true,
   dry: false,
   exclude: []
  }),
  new Extract("css/style.css"),
  //--optimize-minimize (-p)
  new Webpack.optimize.UglifyJsPlugin({
   debug: false,
   minimize: true,
   sourceMap: false,
   output: {
    comments: false
   },
   compressor: {
    warnings: false,
    drop_console: true
   }
  }),
  new Webpack.optimize.OccurrenceOrderPlugin(),
  new Html({
   filename: 'index.html',
   template: 'aindex.html'
  })
 ]
})